package com.eyo.sample.testBehaviour;

public class InvalidSecretTestBehaviour extends TestBehaviour {
	
	/**
	 * Tests the behaviour if the secret is not divisable by 8.
	 */
	
	public InvalidSecretTestBehaviour() {
		super();
		System.setProperty("eyo_plugin_secret", "secretsecretsecretsecretsecret"); //not divisable by 8 -> key can not be generated
	}
	
	

}
