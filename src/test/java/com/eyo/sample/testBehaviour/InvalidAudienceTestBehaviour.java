package com.eyo.sample.testBehaviour;

import org.jose4j.jwt.JwtClaims;

public class InvalidAudienceTestBehaviour extends TestBehaviour {
	
	
	@Override
	public JwtClaims getClaims() {
		JwtClaims claims = super.getClaims();
		claims.setAudience("pam");
		return claims;
	}

}
