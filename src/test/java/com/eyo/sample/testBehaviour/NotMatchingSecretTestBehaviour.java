package com.eyo.sample.testBehaviour;

import java.io.UnsupportedEncodingException;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;

public class NotMatchingSecretTestBehaviour extends TestBehaviour {
	
	public NotMatchingSecretTestBehaviour() {
		super();
		System.setProperty("eyo_plugin_secret", "nononononononononononononononono");
	}
	
	/**
	 * Generates JWT payload from claims and the key generated from the secret.
	 * Then changed the secret so the decryption fails
	 * 
	 * @param claims	The claims which represent the sso-data 
	 * @param key		The key generated from the secret phrase
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws JoseException
	 */
	@Override
	public String generateEncryptedPayload(JwtClaims claims,HmacKey key) throws UnsupportedEncodingException, JoseException {

		//Create the JSON web signature
		JsonWebSignature jws = new JsonWebSignature();

		// The payload of the JWS is JSON content of the JWT Claims
		jws.setPayload(claims.toJson());

		// The JWT is signed using the private key
		jws.setKey(key);

		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);

		//Change the secret passphrase
		System.setProperty("eyo_plugin_secret", "wrongwrongwrongwrongwrongwrongno");
		
		//Create the Jws
		return jws.getCompactSerialization();
	}
	
}
