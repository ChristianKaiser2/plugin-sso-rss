package com.eyo.sample.testBehaviour;

import static org.junit.Assert.assertEquals;

import java.io.UnsupportedEncodingException;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;

import com.eyo.sample.configuration.Configuration;
import com.eyo.sample.sso.Data;

/**
 * Template for various TestBehaviours. Manages the test parameters and validates the outcome.
 * 
 * The template defaults to a positive outcome. This means a valid JWT is created and can be succesfully decrypted and validated.
 * 
 */

public abstract class TestBehaviour {

	/**
	 * Single Constructor for Testbehaviour, initializes System variables
	 */
	public TestBehaviour() {
		System.setProperty("eyo_plugin_secret", "secretsecretsecretsecretsecretse");
		System.setProperty("eyo_plugin_id","map");
	}
	
	public JwtClaims getClaims() {
		//Create the claims for the JWT
		JwtClaims claims = new JwtClaims();

		//Fill the claims with values
		claims.setIssuer(Configuration.getInstance().getPluginId());
		claims.setAudience("map");

		claims.setClaim(Configuration.SSO_INSTANCE_ID, "1");
		claims.setClaim(Configuration.SSO_USER_ID, "ck");
		claims.setClaim(Configuration.SSO_USER_EXTERNAL_ID, "extern.ck");
		claims.setClaim(Configuration.SSO_USER_FIRSTNAME, "Christian");
		claims.setClaim(Configuration.SSO_USER_LASTNAME, "Kaiser");
		claims.setClaim(Configuration.SSO_USER_ROLE, "editor");
		claims.setClaim(Configuration.SSO_USER_LANGUAGE, "en_US");

		return claims;
	}
	
	public HmacKey getKey() {
		return Configuration.getInstance().getKey();
	}
	
	/**
	 * Generates JWT payload from claims and the key generated from the secret
	 * 
	 * @param claims	The claims which represent the sso-data 
	 * @param key		The key generated from the secret phrase
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws JoseException
	 */
	public String generateEncryptedPayload(JwtClaims claims,HmacKey key) throws UnsupportedEncodingException, JoseException {

		//Create the JSON web signature
		JsonWebSignature jws = new JsonWebSignature();

		// The payload of the JWS is JSON content of the JWT Claims
		jws.setPayload(claims.toJson());

		// The JWT is signed using the private key
		jws.setKey(key);

		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);

		//Create the Jws
		
		return jws.getCompactSerialization();
	}
	
	/**
	 * Checks if the decrypted data is equal to the encrypted data
	 * 
	 * @param decryptedData	The data from the decrypted JWT 
	 * @param claims		The claims from the creation of the JWT
	 * @throws MalformedClaimException
	 */
	public void validateDecryptedData(Data decryptedData, JwtClaims claims) throws MalformedClaimException {
		assertEquals(decryptedData.instanceID,claims.getStringClaimValue(Configuration.SSO_INSTANCE_ID));
		assertEquals(decryptedData.userID,claims.getStringClaimValue(Configuration.SSO_USER_ID));
		assertEquals(decryptedData.userExternalID,claims.getStringClaimValue(Configuration.SSO_USER_EXTERNAL_ID));
		assertEquals(decryptedData.userFirstName,claims.getStringClaimValue(Configuration.SSO_USER_FIRSTNAME));
		assertEquals(decryptedData.userLastName,claims.getStringClaimValue(Configuration.SSO_USER_LASTNAME));
		assertEquals(decryptedData.userLocale,claims.getStringClaimValue(Configuration.SSO_USER_LANGUAGE));
		assertEquals(decryptedData.userRole,claims.getStringClaimValue(Configuration.SSO_USER_ROLE));
					
		claims.setIssuer(Configuration.getInstance().getPluginId());
		claims.setAudience("map");		
	}
}
