package com.eyo.sample;

import com.eyo.sample.sso.EyoSSO;
import com.google.inject.AbstractModule;

public class PluginModule extends AbstractModule {

  @Override
  protected void configure() {
    bind(EyoSSO.class).asEagerSingleton();
  }  
}
