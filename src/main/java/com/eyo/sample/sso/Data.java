package com.eyo.sample.sso;

/**
 ** Data contains the transmitted values from the single sign on.
 */

public class Data {
	/** The id of this instance **/
	public final String instanceID;
	/** The user id of the user making the request using SSO**/
	public final String userID;
	/** The external id of the user **/
	public final String userExternalID;
	/** The first name of the user making the request **/
	public final String userFirstName;
	/** The last name of the user making the request **/
	public final String userLastName;
	/** The role of the user making the request to be used by the requested plugin **/
	public final String userRole;
	/** The locale of the user making the request **/
	public final String userLocale;

	/**
	 * Constructor setting all values in data. The values can't be changed afterwards.
	 * 
	 * @param instanceID 	 The id of this instance
	 * @param userID		 The id of the user making the request using sso
	 * @param userExternalID The external user id
	 * @param userFirstName	 The first name of the user making the request
	 * @param userLastName	 The last name of the user making the request
	 * @param userRole		 The role of the user making the request
	 * @param userLocale	 The locale of the user making the request
	 */
	public Data(
				final String instanceID,
				final String userID, final String userExternalID, final String userFirstName,
				final String userLastName, final String userRole, final String userLocale) {
		this.instanceID = instanceID;
		this.userID = userID;
		this.userExternalID = userExternalID;
		this.userFirstName = userFirstName;
		this.userLastName = userLastName;
		this.userRole = userRole;
		this.userLocale = userLocale;
	}

	@Override public String toString() {
		return String.format(
				"EyoSSO.Data{"
						+ "instanceID='%s', userID='%s', userExternalID='%s', "
						+ "userFirstName='%s', userLastName='%s', userRole='%s', userLocale='%s'}",
						instanceID, userID, userExternalID, userFirstName, userLastName, userRole, userLocale);
	}
}
